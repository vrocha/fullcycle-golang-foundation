package main

import "fmt"

func main(){
    salarios := map[string]int{"Vinícius":1000,"João":2000,"Maria":3000}
    fmt.Println(salarios["Vinícius"])
    delete(salarios, "Vinícius")
    fmt.Println(salarios["Vinícius"])

    for nome, salario := range salarios {
        fmt.Printf("O salario de %s é %d\n", nome, salario)
    }

    for _, salario := range salarios { //Blank Identifier. Quando não preciso imprimir o "nome"
        fmt.Printf("O salario é %d\n", salario)
    }
}