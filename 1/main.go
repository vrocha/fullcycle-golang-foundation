package main //Pacote Padrão

const a = "Hello, world! Now with comments" //Variável "a". É uma constante e recebe o valor "Hello, world!"

func main() { //Função principal
    println(a) //Printando o valor de a. Observe que ao final da linha, não é necessário colocar o ;
}