# Como executar? 

- go build main.go
- ./main

Ou

- go run main.go

# Versões utilizadas

- MacOS 14.2.1 (23C71)
- Go go1.22.1 darwin/amd64
- IntelliJ 2023.3 CE Build #IC-232.8660.185