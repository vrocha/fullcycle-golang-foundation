//O Go é uma linguagem fortemente tipada. Se uma variável nascer com um tipo, ela vai morrer com esse tipo.
//O Go não permite você declarar uma variável e não utilizar. A exceção para essa regra são as variáveis globais.

package main

const a = "Hello, world!"

var (
    b bool  =   true
    c int   = 10
    d string    = "Vinícius"
    e float64   = 1.2
)

var f bool

func main() {
    a := "X" // Isso aqui é uma inferência. Estamos criando a variável local a e o GO está inferindo um tipo, no caso é string.
    println(a) // O valor printado aqui é o valor da variável local a (Não da variável no escopo global), tendo em vista que possui precedência.
}

func x() {
}