// Nesse exemplo, vamos discutir o conceito de slices. Slices são arrays de tamanhos variáveis. É possível gerar uma fatia desse array.

package main

import "fmt"

func main() {

    s := []int{10,20,30,40,50,60,70,80,90,100} //Aqui temos uma declaração de um slice que aceita valores do tipo inteiro.
    fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s) //Aqui estamos imprimindo o tamanho do slice len(s), sua capacidade e os valores contidos nesse slice.
    fmt.Printf("len=%d cap=%d %v\n", len(s[:0]), cap(s[:0]), s[:0]) //Aqui estamos fatiando o array e mostrando todos os elementos a esquerda da posição 0.
    fmt.Printf("len=%d cap=%d %v\n", len(s[:4]), cap(s[:4]), s[:4]) //Aqui estamos fatiando o array e mostrando todos os elementos a esquerda da posição 4.
    fmt.Printf("len=%d cap=%d %v\n", len(s[2:]), cap(s[2:]), s[2:]) //Aqui estamos fatiando o array e mostrando todos os elementos a esquerda da posição 4.

    s = append(s, 110) //Aqui estamos incluindo o elemento "110" na última posição do array. Isso provoca um aumento no tamanho do array.
    fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s) //Observe o novo tamanho do slice

}