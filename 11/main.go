// Structs

package main

import "fmt"

type Cliente struct {
	Nome  string
	Idade int
	Ativo bool
}

func main() {
	vinicius := Cliente{
		Nome:  "Vinicius",
		Idade: 28,
		Ativo: true,
	}

	fmt.Printf("Meu nome é %s", vinicius.Nome)
}
