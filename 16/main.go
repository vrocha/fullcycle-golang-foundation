package main

func main() {
	a := 10
	println(&a)

	var ponteiro *int = &a
	*ponteiro = 20
	b := &a
	println(*b)
}
