// Nesse exemplo, vimos a utilização do módulo fmt, que permite que o valor de uma variável seja impresso com a formatação adequada.

package main

import "fmt"

var (
    e float64 = 1.2
)

func main(){
    fmt.Printf("O tipo de E é %T\n", e) // Aqui ele imprime o tipo da variável
    fmt.Printf("O tipo de E é %v", e) // Aqui ele imprime o valor da variável
}