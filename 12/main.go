package main

import "fmt"

type Endereco struct {
	Logradouro string
	Numero     int
	Cidade     string
	Estado     string
}

type Cliente struct {
	Nome  string
	Idade int
	Ativo bool
	Endereco
}

func main() {
	vinicius := Cliente{
		Nome:  "Vinicius",
		Idade: 28,
		Ativo: true,
	}

	vinicius.Cidade = "Belém"
	vinicius.Endereco.Estado = "Pará"

	fmt.Printf("Meu nome é %s", vinicius.Nome)
}
