// Nesse exemplo, faremos a criação de um tipo chamado ID e atribuiremos o valor '1' a variável f, do tipo ID.
package main

type ID int

var f ID = 1

func main() {
    println(f)
}