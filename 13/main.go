package main

import "fmt"

type Endereco struct {
	Logradouro string
	Numero     int
	Cidade     string
	Estado     string
}

type Cliente struct {
	Nome  string
	Idade int
	Ativo bool
	Endereco
}

func (c Cliente) Desativar() {
	c.Ativo = false
}

func main() {
	vinicius := Cliente{
		Nome:  "Vinicius",
		Idade: 28,
		Ativo: true,
	}

	vinicius.Desativar()

	fmt.Println("Meu estado é", vinicius.Ativo)
}
