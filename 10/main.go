//Closures

package main

import (
	"fmt"
)

func main() {
	total := func() int {
		return sum(1, 3, 5, 7, 9, 11)
	}()
	fmt.Println(total)
}

func sum(numeros ...int) int {
	total := 0
	for _, numero := range numeros {
		total += numero
	}
	return total
}
